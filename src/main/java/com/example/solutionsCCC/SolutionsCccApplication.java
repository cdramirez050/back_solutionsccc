package com.example.solutionsCCC;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SolutionsCccApplication {

	public static void main(String[] args) {
		SpringApplication.run(SolutionsCccApplication.class, args);
	}

}
