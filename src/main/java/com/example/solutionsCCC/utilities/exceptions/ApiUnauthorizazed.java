package com.example.solutionsCCC.utilities.exceptions;

//Excepcion personalizada de status 401

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class ApiUnauthorizazed extends Exception{

    public ApiUnauthorizazed ( String message){
        super(message);
    }
}
