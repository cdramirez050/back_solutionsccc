package com.example.solutionsCCC.utilities.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class ApiEmpleadoConflict extends Exception{

    public ApiEmpleadoConflict (String message){
        super(message);
    }
}
