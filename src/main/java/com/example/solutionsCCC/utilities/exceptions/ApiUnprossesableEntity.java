package com.example.solutionsCCC.utilities.exceptions;

//Excepcion personalizada de status 422

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
public class ApiUnprossesableEntity extends Exception{

    public ApiUnprossesableEntity(String message){
        super(message);
    }
}
