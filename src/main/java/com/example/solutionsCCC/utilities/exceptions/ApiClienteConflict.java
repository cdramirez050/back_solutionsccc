package com.example.solutionsCCC.utilities.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class ApiClienteConflict extends Exception{

    public ApiClienteConflict (String message){
        super(message);
    }
}
