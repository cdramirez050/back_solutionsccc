package com.example.solutionsCCC.validator.interfaces;

//Es la interface para la validacion de datos recibidos para la creacion de empleados

import com.example.solutionsCCC.request.EmpeladoSaveRequest;
import com.example.solutionsCCC.request.EmpleadoUpdateRequest;
import com.example.solutionsCCC.utilities.exceptions.ApiUnprossesableEntity;
import org.springframework.stereotype.Service;

@Service
public interface IEmpleadoValidator {

    public void validatorSave(EmpeladoSaveRequest empeladoSaveRequest) throws ApiUnprossesableEntity;
    public void validatorUpdate(EmpleadoUpdateRequest empleadoUpdateRequest) throws ApiUnprossesableEntity;
}
