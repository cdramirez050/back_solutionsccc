package com.example.solutionsCCC.validator;

import com.example.solutionsCCC.request.EmpeladoSaveRequest;
import com.example.solutionsCCC.request.EmpleadoUpdateRequest;
import com.example.solutionsCCC.utilities.exceptions.ApiUnprossesableEntity;
import com.example.solutionsCCC.validator.interfaces.IEmpleadoValidator;

public class EmpleadoValidator implements IEmpleadoValidator {

    @Override
    public void validatorSave(EmpeladoSaveRequest empeladoSaveRequest) throws ApiUnprossesableEntity {

        if(empeladoSaveRequest.getNombres() == null || empeladoSaveRequest.getNombres().isEmpty()){
            message("Los nombres son obligatorios");
        }
        if(empeladoSaveRequest.getNombres().length() < 3){
            message("Nombres incorrectos");
        }
        if(empeladoSaveRequest.getApellidos() == null || empeladoSaveRequest.getApellidos().isEmpty()){
            message("Los apellido son obligatorios");
        }
        if(empeladoSaveRequest.getApellidos().length() < 3){
            message("Los apellidos son incorrectos");
        }
        if(empeladoSaveRequest.getTipoDocumento() == null || empeladoSaveRequest.getTipoDocumento().isEmpty()){
            message("El tipo de documento es obligatorio");
        }
        if(empeladoSaveRequest.getNumeroDocumento() == null || empeladoSaveRequest.getNumeroDocumento().isEmpty()){
            message("El numero de documento es obligatorio");
        }
        if(empeladoSaveRequest.getNumeroDocumento().length() < 3){
            message("El numero de docuemnto es incorrecto");
        }
        if(empeladoSaveRequest.getNumeroTelefono() == null || empeladoSaveRequest.getNumeroTelefono().isEmpty()){
            message("El numero de telefono es obligatorio");
        }
        if(empeladoSaveRequest.getNumeroTelefono().length() < 3){
            message("El numero de telefono es incorrecto");
        }
        if(empeladoSaveRequest.getCargo() == null || empeladoSaveRequest.getCargo().isEmpty()){
            message("el cargo es obligatorio");
        }
        if(empeladoSaveRequest.getCargo().length() < 3){
            message("La cargo es incorrecta");
        }
        if(empeladoSaveRequest.getEmail() == null || empeladoSaveRequest.getEmail().isEmpty()){
            message("El correo es obligatorio");
        }
        if(empeladoSaveRequest.getEmail().length() < 3){
            message("el correo es incorrecto");
        }
        if(empeladoSaveRequest.getNomina() == null || empeladoSaveRequest.getNomina().isEmpty()){
            message("La nomina es obligatorio");
        }
        if(empeladoSaveRequest.getRh() == null || empeladoSaveRequest.getRh().isEmpty()){
            message("el rh es obligatorio");
        }
    }

    @Override
    public void validatorUpdate(EmpleadoUpdateRequest empleadoUpdateRequest) throws ApiUnprossesableEntity {

        if (empleadoUpdateRequest.getNombres() == null || empleadoUpdateRequest.getNombres().isEmpty()) {
            message("Los nombres son obligatorios");
        }
        if(empleadoUpdateRequest.getApellidos() == null || empleadoUpdateRequest.getApellidos().isEmpty()){
            message("Los apellidos son obligatorios");
        }
        if(empleadoUpdateRequest.getNumeroTelefono() == null || empleadoUpdateRequest.getNumeroTelefono().isEmpty()){
            message("El numero de telefono es obligatorio");
        }
        if(empleadoUpdateRequest.getEmail() == null || empleadoUpdateRequest.getEmail().isEmpty()){
            message("el numero de telefono es obligatorio");
        }
        if(empleadoUpdateRequest.getCargo() == null || empleadoUpdateRequest.getCargo().isEmpty()){
            message("el cargo es obligatorio");
        }
    }

    private void message (String message) throws ApiUnprossesableEntity {
        throw new ApiUnprossesableEntity(message);

    }
}
