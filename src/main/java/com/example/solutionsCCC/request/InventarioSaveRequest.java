package com.example.solutionsCCC.request;

import com.example.solutionsCCC.dtos.ProductoDto;

import java.io.Serializable;
import java.util.List;

public class InventarioSaveRequest implements Serializable {

    private String fechaInventario;
    private List<ProductoDto> producto;

    public String getFechaInventario() {
        return fechaInventario;
    }

    public void setFechaInventario(String fechaInventario) {
        this.fechaInventario = fechaInventario;
    }

    public List<ProductoDto> getProducto() {
        return producto;
    }

    public void setProducto(List<ProductoDto> producto) {
        this.producto = producto;
    }
}
