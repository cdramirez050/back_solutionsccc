package com.example.solutionsCCC.request;

import java.io.Serializable;

public class InventarioUpdateRequest implements Serializable {

    private String fechaInventario;

    public String getFechaInventario() {
        return fechaInventario;
    }

    public void setFechaInventario(String fechaInventario) {
        this.fechaInventario = fechaInventario;
    }
}
