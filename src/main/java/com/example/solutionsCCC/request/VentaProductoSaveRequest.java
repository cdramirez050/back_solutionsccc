package com.example.solutionsCCC.request;

import com.example.solutionsCCC.dtos.ProductoDto;

import java.io.Serializable;
import java.util.List;

public class VentaProductoSaveRequest implements Serializable {

    private String cantProducto;
    private String pedidoProductoss;
    private List<ProductoDto> producto;

    public String getCantProducto() {
        return cantProducto;
    }

    public void setCantProducto(String cantProducto) {
        this.cantProducto = cantProducto;
    }

    public List<ProductoDto> getProducto() {
        return producto;
    }

    public void setProducto(List<ProductoDto> producto) {
        this.producto = producto;
    }

    public String getPedidoProductoss() {
        return pedidoProductoss;
    }

    public void setPedidoProductoss(String pedidoProductoss) {
        this.pedidoProductoss = pedidoProductoss;
    }
}
