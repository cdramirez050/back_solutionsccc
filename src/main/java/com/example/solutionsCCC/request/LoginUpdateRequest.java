package com.example.solutionsCCC.request;

import java.io.Serializable;

public class LoginUpdateRequest implements Serializable {

    private String password;
    private Boolean estado;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }
}
