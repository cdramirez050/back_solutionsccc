package com.example.solutionsCCC.request;

import com.example.solutionsCCC.dtos.NominaDto;

import java.io.Serializable;
import java.util.List;

public class EmpleadoUpdateRequest implements Serializable {

    private String nombres;
    private String apellidos;
    private String fechaContrato;
    private String fechaNacimineto;
    private String numeroTelefono;
    private String rh;
    private String cargo;
    private String email;
    private Boolean estado;
    private List<NominaDto> nomina;

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getFechaContrato() {
        return fechaContrato;
    }

    public void setFechaContrato(String fechaContrato) {
        this.fechaContrato = fechaContrato;
    }

    public String getFechaNacimineto() {
        return fechaNacimineto;
    }

    public void setFechaNacimineto(String fechaNacimineto) {
        this.fechaNacimineto = fechaNacimineto;
    }

    public String getNumeroTelefono() {
        return numeroTelefono;
    }

    public void setNumeroTelefono(String numeroTelefono) {
        this.numeroTelefono = numeroTelefono;
    }

    public String getRh() {
        return rh;
    }

    public void setRh(String rh) {
        this.rh = rh;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<NominaDto> getNomina() {
        return nomina;
    }

    public void setNomina(List<NominaDto> nomina) {
        this.nomina = nomina;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }
}
