package com.example.solutionsCCC.request;

import com.example.solutionsCCC.dtos.EmpleadoDto;
import com.example.solutionsCCC.dtos.VentaProductoDto;

import java.io.Serializable;
import java.util.List;

public class ReporteSaveRequest implements Serializable {

    private List<EmpleadoDto> empleadoNomina;
    private List<VentaProductoDto> pedidoProducto;

    public List<EmpleadoDto> getEmpleadoNomina() {
        return empleadoNomina;
    }

    public void setEmpleadoNomina(List<EmpleadoDto> empleadoNomina) {
        this.empleadoNomina = empleadoNomina;
    }

    public List<VentaProductoDto> getPedidoProducto() {
        return pedidoProducto;
    }

    public void setPedidoProducto(List<VentaProductoDto> pedidoProducto) {
        this.pedidoProducto = pedidoProducto;
    }
}
