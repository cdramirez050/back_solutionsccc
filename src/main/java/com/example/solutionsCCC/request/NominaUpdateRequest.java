package com.example.solutionsCCC.request;

import java.io.Serializable;

public class NominaUpdateRequest implements Serializable {

    private String nombre;
    private double totalPagar;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getTotalPagar() {
        return totalPagar;
    }

    public void setTotalPagar(double totalPagar) {
        this.totalPagar = totalPagar;
    }
}
