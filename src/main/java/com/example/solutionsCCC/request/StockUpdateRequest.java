package com.example.solutionsCCC.request;

import com.example.solutionsCCC.entitys.PedidoEntity;
import com.example.solutionsCCC.entitys.VentaProductoEntity;

import java.io.Serializable;
import java.util.List;

public class StockUpdateRequest implements Serializable {

    private List<VentaProductoEntity> producto;
    private List<PedidoEntity> pedido;

    public List<VentaProductoEntity> getProducto() {
        return producto;
    }

    public void setProducto(List<VentaProductoEntity> producto) {
        this.producto = producto;
    }

    public List<PedidoEntity> getPedido() {
        return pedido;
    }

    public void setPedido(List<PedidoEntity> pedido) {
        this.pedido = pedido;
    }

}
