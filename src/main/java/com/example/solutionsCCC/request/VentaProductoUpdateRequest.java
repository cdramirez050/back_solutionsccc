package com.example.solutionsCCC.request;

import java.io.Serializable;

public class VentaProductoUpdateRequest implements Serializable {

    private String cantProducto;
    private String pedidoProductoss;

    public String getCantProducto() {
        return cantProducto;
    }

    public void setCantProducto(String cantProducto) {
        this.cantProducto = cantProducto;
    }

    public String getPedidoProductoss() {
        return pedidoProductoss;
    }

    public void setPedidoProductoss(String pedidoProductoss) {
        this.pedidoProductoss = pedidoProductoss;
    }
}
