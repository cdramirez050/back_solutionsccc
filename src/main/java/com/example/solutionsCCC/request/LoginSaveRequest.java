package com.example.solutionsCCC.request;

import java.io.Serializable;

public class LoginSaveRequest implements Serializable {

    private String usuario;
    private String password;
    private Boolean estado;

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }
}
