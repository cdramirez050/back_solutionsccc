package com.example.solutionsCCC.request;

import com.example.solutionsCCC.dtos.PedidoDto;
import com.example.solutionsCCC.dtos.VentaProductoDto;

import java.io.Serializable;
import java.util.List;

public class StockSaveRequest implements Serializable {

    private List<VentaProductoDto> producto;
    private List<PedidoDto> pedido;


    public List<VentaProductoDto> getProducto() {
        return producto;
    }

    public void setProducto(List<VentaProductoDto> producto) {
        this.producto = producto;
    }

    public List<PedidoDto> getPedido() {
        return pedido;
    }

    public void setPedido(List<PedidoDto> pedido) {
        this.pedido = pedido;
    }

}
