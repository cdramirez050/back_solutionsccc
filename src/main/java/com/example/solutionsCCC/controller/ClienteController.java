package com.example.solutionsCCC.controller;

import com.example.solutionsCCC.request.ClienteSaveRequest;
import com.example.solutionsCCC.request.ClienteUpdateRequest;
import com.example.solutionsCCC.services.interfaces.IClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/cliente")
public class ClienteController {

    @Autowired
    private IClienteService iClienteService;

    @PostMapping
    public ResponseEntity<Object> save(@RequestBody ClienteSaveRequest clienteSaveRequest) {
        iClienteService.save(clienteSaveRequest);
        return ResponseEntity.ok("el cliente se guardo correctamente");
    }

    @PutMapping(value = "{id}")
    public ResponseEntity<Object> update(@RequestBody ClienteUpdateRequest clienteUpdateRequest,
                                         @PathVariable int id){
        return ResponseEntity.ok(iClienteService.update(clienteUpdateRequest, id));
    }

    @DeleteMapping(value = "{id}")
    public ResponseEntity<Object> deleteById(@PathVariable int id){
        iClienteService.deleteById(id);
        return ResponseEntity.ok("El cliente se elimino correctamente");
    }

    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findById(@PathVariable(value = "id") int id){
        return ResponseEntity.ok(iClienteService.findById(id));
    }

    @GetMapping(value = "/numeroDocumento/{numeroDocumento}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findByNumeroDocumento(@PathVariable(value = "numeroDocumento")
                                                                String numeroDocumento) {
        return ResponseEntity.ok(iClienteService.findByNumeroDocumento(numeroDocumento));
    }

    @GetMapping(value = "/nombres/{nombres}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findByNombres(@PathVariable(value = "nombres") String nombres) {
        return ResponseEntity.ok(iClienteService.findByNombres(nombres));
    }

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findAll(){
        return ResponseEntity.ok(iClienteService.findAll());
    }
}
