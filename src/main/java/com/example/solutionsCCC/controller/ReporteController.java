package com.example.solutionsCCC.controller;

import com.example.solutionsCCC.request.ReporteSaveRequest;
import com.example.solutionsCCC.services.interfaces.IReporteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/reporte")
public class ReporteController {

    @Autowired
    private IReporteService iReporteService;

    @PostMapping
    public ResponseEntity<Object> save(@RequestBody ReporteSaveRequest reporteSaveRequest) {
        iReporteService.save(reporteSaveRequest);
        return ResponseEntity.ok("el reporte se guardo correctamente");
    }

    @DeleteMapping(value = "{id}")
    public ResponseEntity<Object> deleteById(@PathVariable int id){
        iReporteService.deleteById(id);
        return ResponseEntity.ok("El reporte se elimino correctamente");
    }

    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findById(@PathVariable(value = "id") int id){
        return ResponseEntity.ok(iReporteService.findById(id));
    }

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findAll(){
        return ResponseEntity.ok(iReporteService.findAll());
    }

}

