package com.example.solutionsCCC.controller;

import com.example.solutionsCCC.request.VentaProductoSaveRequest;
import com.example.solutionsCCC.request.VentaProductoUpdateRequest;
import com.example.solutionsCCC.services.interfaces.IVentaProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/ventaproducto")
public class VentaProductoController {

    @Autowired
    private IVentaProductoService iVentaProductoService;

    @PostMapping
    public ResponseEntity<Object> save(@RequestBody VentaProductoSaveRequest ventaProductoSaveRequest) {
        iVentaProductoService.save(ventaProductoSaveRequest);
        return ResponseEntity.ok("registro exitoso");
    }

    @PutMapping(value = "{id}")
    public ResponseEntity<Object> update(@RequestBody VentaProductoUpdateRequest ventaProductoUpdateRequest,
                                         @PathVariable int id){
        return ResponseEntity.ok(iVentaProductoService.update(ventaProductoUpdateRequest, id));
    }

    @DeleteMapping(value = "{id}")
    public ResponseEntity<Object> deleteById(@PathVariable int id){
        iVentaProductoService.deleteById(id);
        return ResponseEntity.ok("registro eliminado");
    }

    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findById(@PathVariable(value = "id") int id){
        return ResponseEntity.ok(iVentaProductoService.findById(id));
    }

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findAll(){
        return ResponseEntity.ok(iVentaProductoService.findAll());
    }
}
