package com.example.solutionsCCC.controller;

import com.example.solutionsCCC.request.LoginSaveRequest;
import com.example.solutionsCCC.request.LoginUpdateRequest;
import com.example.solutionsCCC.services.interfaces.ILoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/login")
public class LoginController {

    @Autowired
    private ILoginService iLoginService;

    @PostMapping
    public ResponseEntity<Object> save(@RequestBody LoginSaveRequest loginSaveRequest) {
        iLoginService.save(loginSaveRequest);
        return ResponseEntity.ok("");
    }

    @PutMapping(value = "{id}")
    public ResponseEntity<Object> update(@RequestBody LoginUpdateRequest loginUpdateRequest,
                                         @PathVariable int id){
        return ResponseEntity.ok(iLoginService.update(loginUpdateRequest, id));
    }

    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findById(@PathVariable(value = "id") int id){
        return ResponseEntity.ok(iLoginService.findById(id));
    }
}
