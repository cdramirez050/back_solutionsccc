package com.example.solutionsCCC.controller;

import com.example.solutionsCCC.request.InventarioSaveRequest;
import com.example.solutionsCCC.request.InventarioUpdateRequest;
import com.example.solutionsCCC.services.interfaces.IInventarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/inventario")
public class InventarioController {

    @Autowired
    private IInventarioService iInventarioService;

    @PostMapping
    public ResponseEntity<Object> save(@RequestBody InventarioSaveRequest inventarioSaveRequest) {
        iInventarioService.save(inventarioSaveRequest);
        return ResponseEntity.ok("el inventario se guardo correctamente");
    }

    @PutMapping(value = "{id}")
    public ResponseEntity<Object> update(@RequestBody InventarioUpdateRequest inventarioUpdateRequest,
                                         @PathVariable int id){
        return ResponseEntity.ok(iInventarioService.update(inventarioUpdateRequest, id));
    }

    @DeleteMapping(value = "{id}")
    public ResponseEntity<Object> deleteById(@PathVariable int id){
        iInventarioService.deleteById(id);
        return ResponseEntity.ok("El inventario se elimino correctamente");
    }

    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findById(@PathVariable(value = "id") int id){
        return ResponseEntity.ok(iInventarioService.findById(id));
    }

    @GetMapping(value = "/fechainventario/{fechaInventario}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findByFechaInventario(@PathVariable(value = "fechaInventario")
                                                            String fechaInventario) {
        return ResponseEntity.ok(iInventarioService.findByFechaInventario(fechaInventario));
    }

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findAll(){
        return ResponseEntity.ok(iInventarioService.findAll());
    }
}
