package com.example.solutionsCCC.controller;

import com.example.solutionsCCC.request.NominaSaveRequest;
import com.example.solutionsCCC.request.NominaUpdateRequest;
import com.example.solutionsCCC.services.interfaces.INominaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*" )
@RequestMapping("/api/nomina")
public class NominaController {

    @Autowired
    private INominaService iNominaService;

    @PostMapping
    public ResponseEntity<Object> save(@RequestBody NominaSaveRequest nominaSaveRequest) {
        iNominaService.save(nominaSaveRequest);
        return ResponseEntity.ok("La nomina se guardo correctamente");
    }

    @PutMapping(value = "{id}")
    public ResponseEntity<Object> update(@RequestBody NominaUpdateRequest nominaUpdateRequest,
                                         @PathVariable int id){
        return ResponseEntity.ok(iNominaService.update(nominaUpdateRequest, id));
    }

    @DeleteMapping(value = "{id}")
    public ResponseEntity<Object> deleteById(@PathVariable int id){
        iNominaService.deleteById(id);
        return ResponseEntity.ok("La nomina se elimino correctamente");
    }

    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findById(@PathVariable(value = "id") int id){
        return ResponseEntity.ok(iNominaService.findById(id));
    }

    @GetMapping(value = "/nombre/{nombre}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findByNombre(@PathVariable(value = "nombre") String nombre) {
        return ResponseEntity.ok(iNominaService.findByNombre(nombre));
    }

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findAll(){
        return ResponseEntity.ok(iNominaService.findAll());
    }
}
