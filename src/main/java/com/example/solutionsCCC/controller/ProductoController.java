package com.example.solutionsCCC.controller;

import com.example.solutionsCCC.request.ProductoSaveRequest;
import com.example.solutionsCCC.request.ProductoUpdateRequest;
import com.example.solutionsCCC.services.interfaces.IProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/producto")
public class ProductoController {

    @Autowired
    private IProductoService iProductoService;

    @PostMapping
    public ResponseEntity<Object> save(@RequestBody ProductoSaveRequest productoSaveRequest) {
        iProductoService.save(productoSaveRequest);
        return ResponseEntity.ok("el producto se guardo correctamente");
    }

    @PutMapping(value = "{id}")
    public ResponseEntity<Object> update(@RequestBody ProductoUpdateRequest productoUpdateRequest,
                                         @PathVariable int id){
        return ResponseEntity.ok(iProductoService.update(productoUpdateRequest, id));
    }

    @DeleteMapping(value = "{id}")
    public ResponseEntity<Object> deleteById(@PathVariable int id){
        iProductoService.deleteById(id);
        return ResponseEntity.ok("El producto se elimino correctamente");
    }

    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findById(@PathVariable(value = "id") int id){
        return ResponseEntity.ok(iProductoService.findById(id));
    }

    @GetMapping(value = "/nombre/{nombre}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findByNombre(@PathVariable(value = "nombre")
                                                            String nombre) {
        return ResponseEntity.ok(iProductoService.findByNombre(nombre));
    }

    @GetMapping(value = "/fechaCompra/{fechaCompra}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findByFechaCompra(@PathVariable(value = "fechaCompra")
                                                       String fechaCompra) {
        return ResponseEntity.ok(iProductoService.findByFechaCompra(fechaCompra));
    }

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findAll(){
        return ResponseEntity.ok(iProductoService.findAll());
    }
}
