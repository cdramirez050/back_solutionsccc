package com.example.solutionsCCC.controller;

import com.example.solutionsCCC.request.PedidoSaveRequest;
import com.example.solutionsCCC.request.PedidoUpdateRequest;
import com.example.solutionsCCC.services.interfaces.IPedidoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/pedido")
public class PedidoController {

    @Autowired
    private IPedidoService iPedidoService;

    @PostMapping
    public ResponseEntity<Object> save(@RequestBody PedidoSaveRequest pedidoSaveRequest) {
        iPedidoService.save(pedidoSaveRequest);
        return ResponseEntity.ok("el pedido se guardo correctamente");
    }

    @PutMapping(value = "{id}")
    public ResponseEntity<Object> update(@RequestBody PedidoUpdateRequest pedidoUpdateRequest,
                                         @PathVariable int id){
        return ResponseEntity.ok(iPedidoService.update(pedidoUpdateRequest, id));
    }

    @DeleteMapping(value = "{id}")
    public ResponseEntity<Object> deleteById(@PathVariable int id){
        iPedidoService.deleteById(id);
        return ResponseEntity.ok("El pedido se elimino correctamente");
    }

    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findById(@PathVariable(value = "id") int id){
        return ResponseEntity.ok(iPedidoService.findById(id));
    }

    @GetMapping(value = "/fechaPedido/{fechaPedido}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findByFechaPedido(@PathVariable(value = "fechaPedido")
                                                                String fechaPedido) {
        return ResponseEntity.ok(iPedidoService.findByFechaPedido(fechaPedido));
    }

    @GetMapping(value = "/fechaEntrega/{fechaEntrega}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findByFechaEntrega(@PathVariable(value = "fechaEntrega")
                                                            String fechaEntrega) {
        return ResponseEntity.ok(iPedidoService.findByFechaEntrega(fechaEntrega));
    }

    @GetMapping(value = "/descripcion/{descripcion}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findByDescripcion(@PathVariable(value = "descripcion")
                                                             String descripcion) {
        return ResponseEntity.ok(iPedidoService.findByDescripcion(descripcion));
    }

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findAll(){
        return ResponseEntity.ok(iPedidoService.findAll());
    }
}
