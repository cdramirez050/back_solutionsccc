package com.example.solutionsCCC.controller;

import com.example.solutionsCCC.request.StockSaveRequest;
import com.example.solutionsCCC.request.StockUpdateRequest;
import com.example.solutionsCCC.services.interfaces.IStockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/stock")
public class StockController {

    @Autowired
    private IStockService iStockService;

    @PostMapping
    public ResponseEntity<Object> save(@RequestBody StockSaveRequest stockSaveRequest) {
        iStockService.save(stockSaveRequest);
        return ResponseEntity.ok("el stock se guardo correctamente");
    }

    @PutMapping(value = "{id}")
    public ResponseEntity<Object> update(@RequestBody StockUpdateRequest stockUpdateRequest,
                                         @PathVariable int id){
        return ResponseEntity.ok(iStockService.update(stockUpdateRequest, id));
    }

    @DeleteMapping(value = "{id}")
    public ResponseEntity<Object> deleteById(@PathVariable int id){
        iStockService.deleteById(id);
        return ResponseEntity.ok("El stock se elimino correctamente");
    }

    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findById(@PathVariable(value = "id") int id){
        return ResponseEntity.ok(iStockService.findById(id));
    }

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findAll(){
        return ResponseEntity.ok(iStockService.findAll());
    }


}
