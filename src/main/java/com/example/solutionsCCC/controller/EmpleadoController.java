package com.example.solutionsCCC.controller;

import com.example.solutionsCCC.request.EmpeladoSaveRequest;
import com.example.solutionsCCC.request.EmpleadoUpdateRequest;
import com.example.solutionsCCC.services.interfaces.IEmpleadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/empleado")
public class EmpleadoController {

    @Autowired
    private IEmpleadoService iEmpleadoService;

    @PostMapping
    public ResponseEntity<Object> save(@RequestBody EmpeladoSaveRequest empeladoSaveRequest) {
        iEmpleadoService.save(empeladoSaveRequest);
        return ResponseEntity.ok("el empleado se guardo correctamente");
    }

    @PutMapping(value = "{id}")
    public ResponseEntity<Object> update(@RequestBody EmpleadoUpdateRequest empleadoUpdateRequest,
                                         @PathVariable int id){
        return ResponseEntity.ok(iEmpleadoService.update(empleadoUpdateRequest, id));
    }

    @DeleteMapping(value = "{id}")
    public ResponseEntity<Object> deleteById(@PathVariable int id){
        iEmpleadoService.deleteById(id);
        return ResponseEntity.ok("El empleado se elimino correctamente");
    }

    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findById(@PathVariable(value = "id") int id){
        return ResponseEntity.ok(iEmpleadoService.findById(id));
    }

    @GetMapping(value = "/numeroDocumento/{numeroDocumento}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findByNumeroDocumento(@PathVariable(value = "numeroDocumento")
                                                                   String numeroDocumento) {
        return ResponseEntity.ok(iEmpleadoService.findByNumeroDocumento(numeroDocumento));
    }

    @GetMapping(value = "/nombres/{nombres}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findByNombres(@PathVariable(value = "nombres") String nombres) {
        return ResponseEntity.ok(iEmpleadoService.findByNombres(nombres));
    }

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findAll(){
        return ResponseEntity.ok(iEmpleadoService.findAll());
    }
}
