package com.example.solutionsCCC.dtos;

import java.io.Serializable;
import java.util.List;

public class ReporteDto implements Serializable {

    private int idReporte;
    private List<EmpleadoDto> empleadoNomina;
    private List<VentaProductoDto> pedidoProducto;

    public int getIdReporte() {
        return idReporte;
    }

    public void setIdReporte(int idReporte) {
        this.idReporte = idReporte;
    }

    public List<EmpleadoDto> getEmpleadoNomina() {
        return empleadoNomina;
    }

    public void setEmpleadoNomina(List<EmpleadoDto> empleadoNomina) {
        this.empleadoNomina = empleadoNomina;
    }

    public List<VentaProductoDto> getPedidoProducto() {
        return pedidoProducto;
    }

    public void setPedidoProducto(List<VentaProductoDto> pedidoProducto) {
        this.pedidoProducto = pedidoProducto;
    }
}
