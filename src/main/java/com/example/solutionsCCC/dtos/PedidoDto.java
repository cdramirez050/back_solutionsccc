package com.example.solutionsCCC.dtos;

import java.io.Serializable;
import java.util.List;

public class PedidoDto implements Serializable {

    private int idPedido;
    private String fechaPedido;
    private String fechaEntrega;
    private String descripcion;
    private String cantidad;
    private Double valorUnidad;
    private Double valorTotal;
    private boolean isEstado;
    private List<ClienteDto> cliente;

    public int getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(int idPedido) {
        this.idPedido = idPedido;
    }

    public String getFechaPedido() {
        return fechaPedido;
    }

    public void setFechaPedido(String fechaPedido) {
        this.fechaPedido = fechaPedido;
    }

    public String getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(String fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public Double getValorUnidad() {
        return valorUnidad;
    }

    public void setValorUnidad(Double valorUnidad) {
        this.valorUnidad = valorUnidad;
    }

    public Double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public List<ClienteDto> getCliente() {
        return cliente;
    }

    public void setCliente(List<ClienteDto> cliente) {
        this.cliente = cliente;
    }

    public boolean isEstado() {
        return isEstado;
    }

    public void setEstado(boolean estado) {
        isEstado = estado;
    }
}
