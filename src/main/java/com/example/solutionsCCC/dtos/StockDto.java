package com.example.solutionsCCC.dtos;


import java.io.Serializable;
import java.util.List;

public class StockDto implements Serializable {

    private int idStock;
    private List<VentaProductoDto> producto;
    private List<PedidoDto> pedido;

    public int getIdStock() {
        return idStock;
    }

    public void setIdStock(int idStock) {
        this.idStock = idStock;
    }

    public List<VentaProductoDto> getProducto() {
        return producto;
    }

    public void setProducto(List<VentaProductoDto> producto) {
        this.producto = producto;
    }

    public List<PedidoDto> getPedido() {
        return pedido;
    }

    public void setPedido(List<PedidoDto> pedido) {
        this.pedido = pedido;
    }
}
