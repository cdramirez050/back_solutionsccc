package com.example.solutionsCCC.dtos;

import java.io.Serializable;
import java.util.List;

public class InventarioDto implements Serializable {

    private int idInventario;
    private String fechaInventario;
    private List<ProductoDto> producto;

    public int getIdInventario() {
        return idInventario;
    }

    public void setIdInventario(int idInventario) {
        this.idInventario = idInventario;
    }

    public String getFechaInventario() {
        return fechaInventario;
    }

    public void setFechaInventario(String fechaInventario) {
        this.fechaInventario = fechaInventario;
    }

    public List<ProductoDto> getProducto() {
        return producto;
    }

    public void setProducto(List<ProductoDto> producto) {
        this.producto = producto;
    }
}
