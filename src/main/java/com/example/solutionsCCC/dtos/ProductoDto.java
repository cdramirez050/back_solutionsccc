package com.example.solutionsCCC.dtos;

import java.io.Serializable;

public class ProductoDto implements Serializable {

    private int idProducto;
    private String nombre;
    private String descripcion;
    private String cantidad;
    private String fechaCompra;
    private Double vTotalProducto;
    private Double vUnidadCosto;
    private Double vUnidadVenta;

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getFechaCompra() {
        return fechaCompra;
    }

    public void setFechaCompra(String fechaCompra) {
        this.fechaCompra = fechaCompra;
    }

    public Double getvTotalProducto() {
        return vTotalProducto;
    }

    public void setvTotalProducto(Double vTotalProducto) {
        this.vTotalProducto = vTotalProducto;
    }

    public Double getvUnidadCosto() {
        return vUnidadCosto;
    }

    public void setvUnidadCosto(Double vUnidadCosto) {
        this.vUnidadCosto = vUnidadCosto;
    }

    public Double getvUnidadVenta() {
        return vUnidadVenta;
    }

    public void setvUnidadVenta(Double vUnidadVenta) {
        this.vUnidadVenta = vUnidadVenta;
    }
}
