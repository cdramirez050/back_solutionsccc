package com.example.solutionsCCC.dtos;

import java.io.Serializable;

public class LoginDto implements Serializable {

    private int idLoggin;
    private String usuario;
    private String password;
    private Boolean estado;

    public int getIdLoggin() {
        return idLoggin;
    }

    public void setIdLoggin(int idLoggin) {
        this.idLoggin = idLoggin;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }
}
