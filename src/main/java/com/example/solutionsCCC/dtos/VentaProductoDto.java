package com.example.solutionsCCC.dtos;

import java.io.Serializable;
import java.util.List;

public class VentaProductoDto implements Serializable {

    private int idVentaProducto;
    private String cantProducto;
    private String pedidoProductoss;
    private List<ProductoDto> producto;

    public int getIdVentaProducto() {
        return idVentaProducto;
    }

    public void setIdVentaProducto(int idVentaProducto) {
        this.idVentaProducto = idVentaProducto;
    }

    public String getCantProducto() {
        return cantProducto;
    }

    public void setCantProducto(String cantProducto) {
        this.cantProducto = cantProducto;
    }

    public List<ProductoDto> getProducto() {
        return producto;
    }

    public void setProducto(List<ProductoDto> producto) {
        this.producto = producto;
    }

    public String getPedidoProductoss() {
        return pedidoProductoss;
    }

    public void setPedidoProductoss(String pedidoProductoss) {
        this.pedidoProductoss = pedidoProductoss;
    }
}
