package com.example.solutionsCCC.repository;

import com.example.solutionsCCC.entitys.InventarioEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface IInventarioRepository extends JpaRepository<InventarioEntity, Integer> {

    @Transactional
    Optional<InventarioEntity> findByFechaInventario(String fechaInventario);
}
