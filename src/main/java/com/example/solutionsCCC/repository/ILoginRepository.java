package com.example.solutionsCCC.repository;

import com.example.solutionsCCC.entitys.LoginEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ILoginRepository extends JpaRepository<LoginEntity, Integer> {
}
