package com.example.solutionsCCC.repository;

import com.example.solutionsCCC.entitys.NominaEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface INominaRepository extends JpaRepository<NominaEntity, Integer> {

    @Transactional
    Optional<NominaEntity> findByNombre(String nombre);
}
