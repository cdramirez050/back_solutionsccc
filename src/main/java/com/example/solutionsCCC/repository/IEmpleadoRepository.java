package com.example.solutionsCCC.repository;

import com.example.solutionsCCC.entitys.EmpleadoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface IEmpleadoRepository extends JpaRepository<EmpleadoEntity, Integer> {

    @Transactional
    Optional<EmpleadoEntity> findByNumeroDocumento(String numeroDocumento);

    @Transactional
    Optional<EmpleadoEntity> findByNombres(String nombres);
}
