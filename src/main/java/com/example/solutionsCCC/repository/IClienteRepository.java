package com.example.solutionsCCC.repository;

import com.example.solutionsCCC.entitys.ClienteEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface IClienteRepository extends JpaRepository<ClienteEntity, Integer> {

    @Transactional
    Optional<ClienteEntity> findByNumeroDocumento(String numeroDocumento);

    @Transactional
    Optional<ClienteEntity> findByNombres(String nombres);
}
