package com.example.solutionsCCC.repository;

import com.example.solutionsCCC.entitys.VentaProductoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IVentaProductoRepository extends JpaRepository<VentaProductoEntity, Integer> {
}
