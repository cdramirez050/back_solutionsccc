package com.example.solutionsCCC.repository;

import com.example.solutionsCCC.entitys.ProductoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface IProductoRepository extends JpaRepository<ProductoEntity, Integer> {

    @Transactional
    Optional<ProductoEntity> findByNombre(String nombre);

    @Transactional
    Optional<ProductoEntity> findByFechaCompra(String fechaCompra);

}
