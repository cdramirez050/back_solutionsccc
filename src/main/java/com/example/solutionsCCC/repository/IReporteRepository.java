package com.example.solutionsCCC.repository;

import com.example.solutionsCCC.entitys.ReporteEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IReporteRepository extends JpaRepository<ReporteEntity, Integer> {
}
