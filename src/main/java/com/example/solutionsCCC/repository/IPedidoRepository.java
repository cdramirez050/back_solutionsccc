package com.example.solutionsCCC.repository;

import com.example.solutionsCCC.entitys.PedidoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface IPedidoRepository extends JpaRepository<PedidoEntity, Integer> {

    @Transactional
    Optional<PedidoEntity> findByFechaPedido(String fechaPedido);

    @Transactional
    Optional<PedidoEntity> findByFechaEntrega(String fechaEntrega);

    @Transactional
    Optional<PedidoEntity> findByDescripcion(String descripcion);
}
