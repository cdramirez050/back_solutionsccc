package com.example.solutionsCCC.repository;

import com.example.solutionsCCC.entitys.StockEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IStockRepository extends JpaRepository<StockEntity, Integer> {


}
