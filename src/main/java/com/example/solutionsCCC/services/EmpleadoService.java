package com.example.solutionsCCC.services;

import com.example.solutionsCCC.configuration.MoMapper;
import com.example.solutionsCCC.dtos.EmpleadoDto;
import com.example.solutionsCCC.entitys.EmpleadoEntity;
import com.example.solutionsCCC.repository.IEmpleadoRepository;
import com.example.solutionsCCC.request.EmpeladoSaveRequest;
import com.example.solutionsCCC.request.EmpleadoUpdateRequest;
import com.example.solutionsCCC.services.interfaces.IEmpleadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class EmpleadoService implements IEmpleadoService {

    @Autowired
    private IEmpleadoRepository iEmpleadoRepository;


    @Override
    public void save(EmpeladoSaveRequest empeladoSaveRequest) {
        EmpleadoEntity empleadoEntity = MoMapper.modelMapper().map(empeladoSaveRequest, EmpleadoEntity.class);
        iEmpleadoRepository.save(empleadoEntity);
    }

    @Override
    public void deleteById(int id) {
        iEmpleadoRepository.deleteById(id);
    }


    @Override
    public EmpleadoDto update(EmpleadoUpdateRequest empleadoUpdateRequest, int id) {
        Optional<EmpleadoEntity> optionalEmpleadoEntity = iEmpleadoRepository.findById(id);
        if(optionalEmpleadoEntity.isPresent()){
            EmpleadoEntity empleadoEntity = optionalEmpleadoEntity.get();
            empleadoEntity.setNombres(empleadoUpdateRequest.getNombres());
            empleadoEntity.setApellidos(empleadoUpdateRequest.getApellidos());
            empleadoEntity.setEmail(empleadoUpdateRequest.getEmail());
            empleadoEntity.setNumeroTelefono(empleadoUpdateRequest.getNumeroTelefono());
            empleadoEntity.setCargo(empleadoUpdateRequest.getCargo());
            empleadoEntity.setFechaContrato(empleadoUpdateRequest.getFechaContrato());
            empleadoEntity.setFechaNacimineto(empleadoUpdateRequest.getFechaNacimineto());
            empleadoEntity.setEstado(empleadoUpdateRequest.getEstado());
            empleadoEntity.setRh(empleadoUpdateRequest.getRh());
            iEmpleadoRepository.save(empleadoEntity);
            return MoMapper.modelMapper().map(empleadoEntity, EmpleadoDto.class);
        }
        return null;
    }

    @Override
    public EmpleadoDto findById(int id) {
        Optional<EmpleadoEntity> optionalEmpleadoEntity = iEmpleadoRepository.findById(id);
        EmpleadoDto empleadoDto = null;
        if(optionalEmpleadoEntity.isPresent()){
            empleadoDto = MoMapper.modelMapper().map(optionalEmpleadoEntity.get(), EmpleadoDto.class);
        }
        return empleadoDto;
    }

    @Override
    public EmpleadoDto findByNumeroDocumento(String numeroDocumento) {
        Optional<EmpleadoEntity> empleado = iEmpleadoRepository.findByNumeroDocumento(numeroDocumento);
        EmpleadoDto empleadoDto = null;
        if (empleado.isPresent()) {
            empleadoDto = MoMapper.modelMapper().map(empleado.get(), EmpleadoDto.class);
        }
        return empleadoDto;
    }

    @Override
    public EmpleadoDto findByNombres(String nombres) {
        Optional<EmpleadoEntity> empleado = iEmpleadoRepository.findByNombres(nombres);
        EmpleadoDto empleadoDto = null;
        if (empleado.isPresent()) {
            empleadoDto = MoMapper.modelMapper().map(empleado.get(), EmpleadoDto.class);
        }
        return empleadoDto;
    }

    @Override
    public List<EmpleadoDto> findAll() {
        List<EmpleadoDto> dtoList = new ArrayList<>();
        Iterable<EmpleadoEntity> empleadoEntities = iEmpleadoRepository.findAll();
        for(EmpleadoEntity empleadoEntity: empleadoEntities){
            EmpleadoDto empleadoDto = MoMapper.modelMapper().map(empleadoEntity, EmpleadoDto.class);
            dtoList.add(empleadoDto);
        }
        return dtoList;
    }

    private EmpleadoDto convertToEmpleadoDto(final EmpleadoEntity empleadoEntity) {
        return MoMapper.modelMapper().map(empleadoEntity, EmpleadoDto.class);
    }
}
