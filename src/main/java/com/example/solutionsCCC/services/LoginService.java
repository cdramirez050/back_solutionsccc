package com.example.solutionsCCC.services;

import com.example.solutionsCCC.configuration.MoMapper;
import com.example.solutionsCCC.dtos.LoginDto;
import com.example.solutionsCCC.dtos.NominaDto;
import com.example.solutionsCCC.entitys.LoginEntity;
import com.example.solutionsCCC.entitys.NominaEntity;
import com.example.solutionsCCC.repository.ILoginRepository;
import com.example.solutionsCCC.repository.INominaRepository;
import com.example.solutionsCCC.request.LoginSaveRequest;
import com.example.solutionsCCC.request.LoginUpdateRequest;
import com.example.solutionsCCC.request.NominaSaveRequest;
import com.example.solutionsCCC.request.NominaUpdateRequest;
import com.example.solutionsCCC.services.interfaces.ILoginService;
import com.example.solutionsCCC.services.interfaces.INominaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LoginService implements ILoginService {

    @Autowired
    private ILoginRepository iLoginRepository;


    @Override
    public void save(LoginSaveRequest loginSaveRequest) {
        LoginEntity loginEntity = MoMapper.modelMapper().map(loginSaveRequest, LoginEntity.class);
        iLoginRepository.save(loginEntity);
    }

    @Override
    public LoginDto update(LoginUpdateRequest loginUpdateRequest, int id) {
        Optional<LoginEntity> optionalLoginEntity = iLoginRepository.findById(id);
        if(optionalLoginEntity.isPresent()){
            LoginEntity loginEntity = optionalLoginEntity.get();
            loginEntity.setPassword(loginUpdateRequest.getPassword());
            loginEntity.setEstado(loginUpdateRequest.getEstado());
            iLoginRepository.save(loginEntity);
            return MoMapper.modelMapper().map(loginEntity, LoginDto.class);
        }
        return null;
    }

    public LoginDto findById(int id) {
        Optional<LoginEntity> optionalLoginEntity = iLoginRepository.findById(id);
        LoginDto loginDto = null;
        if(optionalLoginEntity.isPresent()){
            loginDto = MoMapper.modelMapper().map(optionalLoginEntity.get(), LoginDto.class);
        }
        return loginDto;
    }

    private LoginDto convertToLoginDto(final LoginEntity loginEntity) {
        return MoMapper.modelMapper().map(loginEntity, LoginDto.class);
    }

}
