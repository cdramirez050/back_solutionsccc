package com.example.solutionsCCC.services;

import com.example.solutionsCCC.configuration.MoMapper;
import com.example.solutionsCCC.dtos.InventarioDto;
import com.example.solutionsCCC.entitys.InventarioEntity;
import com.example.solutionsCCC.repository.IInventarioRepository;
import com.example.solutionsCCC.request.InventarioSaveRequest;
import com.example.solutionsCCC.request.InventarioUpdateRequest;
import com.example.solutionsCCC.services.interfaces.IInventarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class InventarioService implements IInventarioService {

    @Autowired
    private IInventarioRepository iInventarioRepository;

    @Override
    public void save(InventarioSaveRequest inventarioSaveRequest) {
        InventarioEntity inventarioEntity = MoMapper.modelMapper().map(inventarioSaveRequest, InventarioEntity.class);
        iInventarioRepository.save(inventarioEntity);
    }

    @Override
    public void deleteById(int id) {
        iInventarioRepository.deleteById(id);
    }

    @Override
    public InventarioDto update(InventarioUpdateRequest inventarioUpdateRequest, int id) {
        Optional<InventarioEntity> optionalInventarioEntity = iInventarioRepository.findById(id);
        if(optionalInventarioEntity.isPresent()){
            InventarioEntity inventarioEntity = optionalInventarioEntity.get();
            inventarioEntity.setFechaInventario(inventarioUpdateRequest.getFechaInventario());
            iInventarioRepository.save(inventarioEntity);
            return MoMapper.modelMapper().map(inventarioEntity, InventarioDto.class);
        }
        return null;
    }

    @Override
    public InventarioDto findById(int id) {
        Optional<InventarioEntity> optionalInventarioEntity = iInventarioRepository.findById(id);
        InventarioDto inventarioDto = null;
        if(optionalInventarioEntity.isPresent()){
            inventarioDto = MoMapper.modelMapper().map(optionalInventarioEntity.get(), InventarioDto.class);
        }
        return inventarioDto;
    }

    @Override
    public InventarioDto findByFechaInventario(String fechaInventario) {
        Optional<InventarioEntity> inventarioEntity = iInventarioRepository.findByFechaInventario(fechaInventario);
        InventarioDto inventarioDto = null;
        if (inventarioEntity.isPresent()) {
            inventarioDto = MoMapper.modelMapper().map(inventarioEntity.get(), InventarioDto.class);
        }
        return inventarioDto;
    }

    @Override
    public List<InventarioDto> findAll() {
        List<InventarioDto> dtoList = new ArrayList<>();
        Iterable<InventarioEntity> inventarioEntities = iInventarioRepository.findAll();
        for(InventarioEntity inventarioEntity: inventarioEntities){
            InventarioDto inventarioDto = MoMapper.modelMapper().map(inventarioEntity, InventarioDto.class);
            dtoList.add(inventarioDto);
        }
        return dtoList;
    }

    private InventarioDto convertToInventarioDto(final InventarioEntity inventarioEntity) {
        return MoMapper.modelMapper().map(inventarioEntity, InventarioDto.class);
    }
}
