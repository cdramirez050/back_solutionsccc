package com.example.solutionsCCC.services;

import com.example.solutionsCCC.configuration.MoMapper;
import com.example.solutionsCCC.dtos.StockDto;
import com.example.solutionsCCC.entitys.StockEntity;
import com.example.solutionsCCC.repository.IStockRepository;
import com.example.solutionsCCC.request.StockSaveRequest;
import com.example.solutionsCCC.request.StockUpdateRequest;
import com.example.solutionsCCC.services.interfaces.IStockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class StockService implements IStockService {

    @Autowired
    private IStockRepository iStockRepository;

    @Override
    public void save(StockSaveRequest stockSaveRequest) {
        StockEntity stockEntity = MoMapper.modelMapper().map(stockSaveRequest, StockEntity.class);
        iStockRepository.save(stockEntity);
    }

    @Override
    public void deleteById(int id) {
        iStockRepository.deleteById(id);
    }

    @Override
    public StockDto update(StockUpdateRequest stockUpdateRequest, int id) {
        Optional<StockEntity> optionalStockEntity = iStockRepository.findById(id);
        if(optionalStockEntity.isPresent()){
            StockEntity stockEntity = optionalStockEntity.get();
            stockEntity.setPedido(stockUpdateRequest.getPedido());
            stockEntity.setProducto(stockUpdateRequest.getProducto());
            iStockRepository.save(stockEntity);
            return MoMapper.modelMapper().map(stockEntity, StockDto.class);
        }
        return null;
    }

    @Override
    public StockDto findById(int id) {
        Optional<StockEntity> optionalStockEntity = iStockRepository.findById(id);
        StockDto stockDto = null;
        if(optionalStockEntity.isPresent()){
            stockDto = MoMapper.modelMapper().map(optionalStockEntity.get(), StockDto.class);
        }
        return stockDto;
    }

    @Override
    public List<StockDto> findAll() {
        List<StockDto> dtoList = new ArrayList<>();
        Iterable<StockEntity> stockEntities = iStockRepository.findAll();
        for(StockEntity stockEntity: stockEntities){
            StockDto stockDto = MoMapper.modelMapper().map(stockEntity, StockDto.class);
            dtoList.add(stockDto);
        }
        return dtoList;
    }

    private StockDto convertToStockDto(final StockEntity stockEntity) {
        return MoMapper.modelMapper().map(stockEntity, StockDto.class);
    }
}
