package com.example.solutionsCCC.services;

import com.example.solutionsCCC.configuration.MoMapper;
import com.example.solutionsCCC.dtos.PedidoDto;
import com.example.solutionsCCC.entitys.PedidoEntity;
import com.example.solutionsCCC.repository.IPedidoRepository;
import com.example.solutionsCCC.request.PedidoSaveRequest;
import com.example.solutionsCCC.request.PedidoUpdateRequest;
import com.example.solutionsCCC.services.interfaces.IPedidoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PedidoService implements IPedidoService {

    @Autowired
    private IPedidoRepository iPedidoRepository;

    @Override
    public void save(PedidoSaveRequest pedidoSaveRequest) {
        PedidoEntity pedidoEntity = MoMapper.modelMapper().map(pedidoSaveRequest, PedidoEntity.class);
        iPedidoRepository.save(pedidoEntity);
    }

    @Override
    public void deleteById(int id) {
        iPedidoRepository.deleteById(id);
    }

    @Override
    public PedidoDto update(PedidoUpdateRequest pedidoUpdateRequest, int id) {
        Optional<PedidoEntity> optionalPedidoEntity = iPedidoRepository.findById(id);
        if(optionalPedidoEntity.isPresent()){
            PedidoEntity pedidoEntity = optionalPedidoEntity.get();
            pedidoEntity.setFechaPedido(pedidoUpdateRequest.getFechaPedido());
            pedidoEntity.setFechaEntrega(pedidoUpdateRequest.getFechaEntrega());
            pedidoEntity.setDescripcion(pedidoUpdateRequest.getDescripcion());
            pedidoEntity.setValorUnidad(pedidoUpdateRequest.getValorUnidad());
            pedidoEntity.setValorTotal(pedidoUpdateRequest.getValorTotal());
            pedidoEntity.setEstado(pedidoUpdateRequest.isEstado());
            iPedidoRepository.save(pedidoEntity);
            return MoMapper.modelMapper().map(pedidoEntity, PedidoDto.class);
        }
        return null;
    }

    @Override
    public PedidoDto findById(int id) {
        Optional<PedidoEntity> optionalPedidoEntity = iPedidoRepository.findById(id);
        PedidoDto pedidoDto = null;
        if(optionalPedidoEntity.isPresent()){
            pedidoDto = MoMapper.modelMapper().map(optionalPedidoEntity.get(), PedidoDto.class);
        }
        return pedidoDto;
    }

    @Override
    public PedidoDto findByFechaPedido(String fechaPedido) {
        Optional<PedidoEntity> pedidoEntity = iPedidoRepository.findByFechaPedido(fechaPedido);
        PedidoDto pedidoDto = null;
        if (pedidoEntity.isPresent()) {
            pedidoDto = MoMapper.modelMapper().map(pedidoEntity.get(), PedidoDto.class);
        }
        return pedidoDto;
    }

    @Override
    public PedidoDto findByFechaEntrega(String fechaEntrega) {
        Optional<PedidoEntity> pedidoEntity = iPedidoRepository.findByFechaEntrega(fechaEntrega);
        PedidoDto pedidoDto = null;
        if (pedidoEntity.isPresent()) {
            pedidoDto = MoMapper.modelMapper().map(pedidoEntity.get(), PedidoDto.class);
        }
        return pedidoDto;
    }

    @Override
    public PedidoDto findByDescripcion(String descripcion) {
        Optional<PedidoEntity> pedidoEntity = iPedidoRepository.findByDescripcion(descripcion);
        PedidoDto pedidoDto = null;
        if (pedidoEntity.isPresent()) {
            pedidoDto = MoMapper.modelMapper().map(pedidoEntity.get(), PedidoDto.class);
        }
        return pedidoDto;
    }

    @Override
    public List<PedidoDto> findAll() {
        List<PedidoDto> dtoList = new ArrayList<>();
        Iterable<PedidoEntity> pedidoEntities = iPedidoRepository.findAll();
        for(PedidoEntity pedidoEntity: pedidoEntities){
            PedidoDto pedidoDto = MoMapper.modelMapper().map(pedidoEntity, PedidoDto.class);
            dtoList.add(pedidoDto);
        }
        return dtoList;
    }

    private PedidoDto convertToPedidoDto(final PedidoEntity pedidoEntity) {
        return MoMapper.modelMapper().map(pedidoEntity, PedidoDto.class);
    }
}
