package com.example.solutionsCCC.services;

import com.example.solutionsCCC.configuration.MoMapper;
import com.example.solutionsCCC.dtos.ProductoDto;
import com.example.solutionsCCC.entitys.ProductoEntity;
import com.example.solutionsCCC.repository.IProductoRepository;
import com.example.solutionsCCC.request.ProductoSaveRequest;
import com.example.solutionsCCC.request.ProductoUpdateRequest;
import com.example.solutionsCCC.services.interfaces.IProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductoService implements IProductoService {

    @Autowired
    private IProductoRepository iProductoRepository;

    @Override
    public void save(ProductoSaveRequest productoSaveRequest) {
        ProductoEntity productoEntity = MoMapper.modelMapper().map(productoSaveRequest, ProductoEntity.class);
        iProductoRepository.save(productoEntity);
    }

    @Override
    public void deleteById(int id) {
        iProductoRepository.deleteById(id);
    }

    @Override
    public ProductoDto update(ProductoUpdateRequest productoUpdateRequest, int id) {
        Optional<ProductoEntity> optionalProductoEntity = iProductoRepository.findById(id);
        if(optionalProductoEntity.isPresent()){
            ProductoEntity productoEntity = optionalProductoEntity.get();
            productoEntity.setNombre(productoUpdateRequest.getNombre());
            productoEntity.setDescripcion(productoUpdateRequest.getDescripcion());
            productoEntity.setCantidad(productoUpdateRequest.getCantidad());
            productoEntity.setFechaCompra(productoUpdateRequest.getFechaCompra());
            productoEntity.setvTotalProducto(productoUpdateRequest.getvTotalProducto());
            productoEntity.setvUnidadCosto(productoUpdateRequest.getvUnidadCosto());
            productoEntity.setvUnidadVenta(productoUpdateRequest.getvUnidadVenta());
            iProductoRepository.save(productoEntity);
            return MoMapper.modelMapper().map(productoEntity, ProductoDto.class);
        }
        return null;
    }

    @Override
    public ProductoDto findById(int id) {
        Optional<ProductoEntity> optionalProductoEntity = iProductoRepository.findById(id);
        ProductoDto productoDto = null;
        if(optionalProductoEntity.isPresent()){
            productoDto = MoMapper.modelMapper().map(optionalProductoEntity.get(), ProductoDto.class);
        }
        return productoDto;
    }

    @Override
    public ProductoDto findByNombre(String nombre) {
        Optional<ProductoEntity> productoEntity = iProductoRepository.findByNombre(nombre);
        ProductoDto productoDto = null;
        if (productoEntity.isPresent()) {
            productoDto = MoMapper.modelMapper().map(productoEntity.get(), ProductoDto.class);
        }
        return productoDto;
    }

    @Override
    public ProductoDto findByFechaCompra(String fechaCompra) {
        Optional<ProductoEntity> productoEntity = iProductoRepository.findByFechaCompra(fechaCompra);
        ProductoDto productoDto = null;
        if (productoEntity.isPresent()) {
            productoDto = MoMapper.modelMapper().map(productoEntity.get(), ProductoDto.class);
        }
        return productoDto;
    }

    @Override
    public List<ProductoDto> findAll() {
        List<ProductoDto> dtoList = new ArrayList<>();
        Iterable<ProductoEntity> productoEntities = iProductoRepository.findAll();
        for(ProductoEntity productoEntity: productoEntities){
            ProductoDto productoDto = MoMapper.modelMapper().map(productoEntity, ProductoDto.class);
            dtoList.add(productoDto);
        }
        return dtoList;
    }

    private ProductoDto convertToProductoDto(final ProductoEntity productoEntity) {
        return MoMapper.modelMapper().map(productoEntity, ProductoDto.class);
    }
}
