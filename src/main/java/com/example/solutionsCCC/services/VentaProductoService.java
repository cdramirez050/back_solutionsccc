package com.example.solutionsCCC.services;

import com.example.solutionsCCC.configuration.MoMapper;
import com.example.solutionsCCC.dtos.VentaProductoDto;
import com.example.solutionsCCC.entitys.VentaProductoEntity;
import com.example.solutionsCCC.repository.IVentaProductoRepository;
import com.example.solutionsCCC.request.VentaProductoSaveRequest;
import com.example.solutionsCCC.request.VentaProductoUpdateRequest;
import com.example.solutionsCCC.services.interfaces.IVentaProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class VentaProductoService implements IVentaProductoService {

    @Autowired
    private IVentaProductoRepository iVentaProductoRepository;

    @Override
    public void save(VentaProductoSaveRequest ventaProductoSaveRequest) {
        VentaProductoEntity ventaProductoEntity = MoMapper.modelMapper()
                .map(ventaProductoSaveRequest, VentaProductoEntity.class);
        iVentaProductoRepository.save(ventaProductoEntity);
    }

    @Override
    public void deleteById(int id) {
        iVentaProductoRepository.deleteById(id);
    }

    @Override
    public VentaProductoDto update(VentaProductoUpdateRequest ventaProductoUpdateRequest, int id) {
        Optional<VentaProductoEntity> optionalVentaProductoEntity = iVentaProductoRepository.findById(id);
        if(optionalVentaProductoEntity.isPresent()){
            VentaProductoEntity ventaProductoEntity = optionalVentaProductoEntity.get();
            ventaProductoEntity.setCantProducto(ventaProductoUpdateRequest.getCantProducto());
            ventaProductoEntity.setPedidoProductoss(ventaProductoUpdateRequest.getPedidoProductoss());
            iVentaProductoRepository.save(ventaProductoEntity);
            return MoMapper.modelMapper().map(ventaProductoEntity, VentaProductoDto.class);
        }
        return null;
    }

    @Override
    public VentaProductoDto findById(int id) {
        Optional<VentaProductoEntity> optionalVentaProductoEntity = iVentaProductoRepository.findById(id);
        VentaProductoDto ventaProductoDto = null;
        if(optionalVentaProductoEntity.isPresent()){
            ventaProductoDto = MoMapper.modelMapper().map(optionalVentaProductoEntity.get(), VentaProductoDto.class);
        }
        return ventaProductoDto;
    }

    @Override
    public List<VentaProductoDto> findAll() {
        List<VentaProductoDto> dtoList = new ArrayList<>();
        Iterable<VentaProductoEntity> ventaProductoEntities = iVentaProductoRepository.findAll();
        for(VentaProductoEntity ventaProductoEntity: ventaProductoEntities){
            VentaProductoDto ventaProductoDto = MoMapper.modelMapper()
                    .map(ventaProductoEntity, VentaProductoDto.class);
            dtoList.add(ventaProductoDto);
        }
        return dtoList;
    }

    private VentaProductoDto convertToVentaProductodto(final VentaProductoEntity ventaProductoEntity) {
        return MoMapper.modelMapper().map(ventaProductoEntity, VentaProductoDto.class);
    }
}
