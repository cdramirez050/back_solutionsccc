package com.example.solutionsCCC.services.interfaces;

import com.example.solutionsCCC.dtos.EmpleadoDto;
import com.example.solutionsCCC.request.EmpeladoSaveRequest;
import com.example.solutionsCCC.request.EmpleadoUpdateRequest;

import java.util.List;

public interface IEmpleadoService {

    void save(EmpeladoSaveRequest empeladoSaveRequest);

    void deleteById(int id);

    EmpleadoDto update(EmpleadoUpdateRequest empleadoUpdateRequest, int id);

    EmpleadoDto findById(int id);

    EmpleadoDto findByNumeroDocumento(String numeroDocumento);

    EmpleadoDto findByNombres(String nombres);

    List<EmpleadoDto> findAll();
}
