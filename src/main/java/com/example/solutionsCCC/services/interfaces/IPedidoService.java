package com.example.solutionsCCC.services.interfaces;

import com.example.solutionsCCC.dtos.PedidoDto;
import com.example.solutionsCCC.request.PedidoSaveRequest;
import com.example.solutionsCCC.request.PedidoUpdateRequest;

import java.util.List;

public interface IPedidoService {

    void save(PedidoSaveRequest pedidoSaveRequest);

    void deleteById(int id);

    PedidoDto update(PedidoUpdateRequest pedidoUpdateRequest, int id);

    PedidoDto findById(int id);

    PedidoDto findByFechaPedido(String fechaPedido);

    PedidoDto findByFechaEntrega(String fechaEntrega);

    PedidoDto findByDescripcion(String descripcion);

    List<PedidoDto> findAll();
}
