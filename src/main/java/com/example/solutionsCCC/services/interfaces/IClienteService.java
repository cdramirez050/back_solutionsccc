package com.example.solutionsCCC.services.interfaces;

import com.example.solutionsCCC.dtos.ClienteDto;
import com.example.solutionsCCC.request.ClienteSaveRequest;
import com.example.solutionsCCC.request.ClienteUpdateRequest;

import java.util.List;

public interface IClienteService {

    void save(ClienteSaveRequest clienteSaveRequest);

    void deleteById(int id);

    ClienteDto update(ClienteUpdateRequest clienteUpdateRequest, int id);

    ClienteDto findById(int id);

    ClienteDto findByNumeroDocumento(String numeroDocumento);

    ClienteDto findByNombres(String nombres);

    List<ClienteDto> findAll();

}
