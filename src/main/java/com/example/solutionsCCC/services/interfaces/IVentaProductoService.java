package com.example.solutionsCCC.services.interfaces;

import com.example.solutionsCCC.dtos.VentaProductoDto;
import com.example.solutionsCCC.request.VentaProductoSaveRequest;
import com.example.solutionsCCC.request.VentaProductoUpdateRequest;

import java.util.List;

public interface IVentaProductoService {

    void save(VentaProductoSaveRequest ventaProductoSaveRequest);

    void deleteById(int id);

    VentaProductoDto update(VentaProductoUpdateRequest ventaProductoUpdateRequest, int id);

    VentaProductoDto findById(int id);

    List<VentaProductoDto> findAll();
}
