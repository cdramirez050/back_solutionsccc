package com.example.solutionsCCC.services.interfaces;

import com.example.solutionsCCC.dtos.InventarioDto;
import com.example.solutionsCCC.request.InventarioSaveRequest;
import com.example.solutionsCCC.request.InventarioUpdateRequest;
import java.util.List;

public interface IInventarioService {

    void save(InventarioSaveRequest inventarioSaveRequest);

    void deleteById(int id);

    InventarioDto update(InventarioUpdateRequest inventarioUpdateRequest, int id);

    InventarioDto findById(int id);

    InventarioDto findByFechaInventario(String fechaInventario);

    List<InventarioDto> findAll();
}
