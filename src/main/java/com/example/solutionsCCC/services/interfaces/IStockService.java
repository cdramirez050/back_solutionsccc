package com.example.solutionsCCC.services.interfaces;

import com.example.solutionsCCC.dtos.StockDto;
import com.example.solutionsCCC.request.StockSaveRequest;
import com.example.solutionsCCC.request.StockUpdateRequest;

import java.util.List;

public interface IStockService {

    void save(StockSaveRequest stockSaveRequest);

    void deleteById(int id);

    StockDto update(StockUpdateRequest stockUpdateRequest, int id);

    StockDto findById(int id);

    List<StockDto> findAll();
}
