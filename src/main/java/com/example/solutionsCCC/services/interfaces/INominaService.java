package com.example.solutionsCCC.services.interfaces;

import com.example.solutionsCCC.dtos.NominaDto;
import com.example.solutionsCCC.request.NominaSaveRequest;
import com.example.solutionsCCC.request.NominaUpdateRequest;

import java.util.List;

public interface INominaService {

    void save(NominaSaveRequest nominaSaveRequest);

    NominaDto update(NominaUpdateRequest nominaUpdateRequest, int id);

    void deleteById(int id);

    NominaDto findById(int id);

    NominaDto findByNombre(String nombre);

    List<NominaDto> findAll();
}
