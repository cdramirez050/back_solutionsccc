package com.example.solutionsCCC.services.interfaces;

import com.example.solutionsCCC.dtos.LoginDto;
import com.example.solutionsCCC.request.LoginSaveRequest;
import com.example.solutionsCCC.request.LoginUpdateRequest;

public interface ILoginService {

    void save(LoginSaveRequest loginSaveRequest);

    LoginDto update(LoginUpdateRequest loginUpdateRequest, int id);

    LoginDto findById(int id);

}
