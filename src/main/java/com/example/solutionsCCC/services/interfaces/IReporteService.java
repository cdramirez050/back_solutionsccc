package com.example.solutionsCCC.services.interfaces;

import com.example.solutionsCCC.dtos.ReporteDto;
import com.example.solutionsCCC.request.ReporteSaveRequest;

import java.util.List;

public interface IReporteService {

    void save(ReporteSaveRequest reporteSaveRequest);

    void deleteById(int id);

    ReporteDto findById(int id);

    List<ReporteDto> findAll();
}
