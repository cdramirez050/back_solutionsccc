package com.example.solutionsCCC.services.interfaces;

import com.example.solutionsCCC.dtos.ProductoDto;
import com.example.solutionsCCC.request.ProductoSaveRequest;
import com.example.solutionsCCC.request.ProductoUpdateRequest;

import java.util.List;

public interface IProductoService {

    void save(ProductoSaveRequest productoSaveRequest);

    void deleteById(int id);

    ProductoDto update(ProductoUpdateRequest productoUpdateRequest, int id);

    ProductoDto findById(int id);

    ProductoDto findByNombre(String nombre);

    ProductoDto findByFechaCompra(String fechaCompra);

    List<ProductoDto> findAll();
}
