package com.example.solutionsCCC.services;

import com.example.solutionsCCC.configuration.MoMapper;
import com.example.solutionsCCC.dtos.ReporteDto;
import com.example.solutionsCCC.entitys.ReporteEntity;
import com.example.solutionsCCC.repository.IReporteRepository;
import com.example.solutionsCCC.request.ReporteSaveRequest;
import com.example.solutionsCCC.services.interfaces.IReporteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ReporteService implements IReporteService {

    @Autowired
    private IReporteRepository iReporteRepository;

    @Override
    public void save(ReporteSaveRequest reporteSaveRequest) {
        ReporteEntity reporteEntity = MoMapper.modelMapper().map(reporteSaveRequest, ReporteEntity.class);
        iReporteRepository.save(reporteEntity);
    }

    @Override
    public void deleteById(int id) {
        iReporteRepository.deleteById(id);
    }

    @Override
    public ReporteDto findById(int id) {
        Optional<ReporteEntity> optionalReporteEntity = iReporteRepository.findById(id);
        ReporteDto reporteDto = null;
        if(optionalReporteEntity.isPresent()){
            reporteDto = MoMapper.modelMapper().map(optionalReporteEntity.get(), ReporteDto.class);
        }
        return reporteDto;
    }

    @Override
    public List<ReporteDto> findAll() {
        List<ReporteDto> dtoList = new ArrayList<>();
        Iterable<ReporteEntity> reporteEntities = iReporteRepository.findAll();
        for(ReporteEntity reporteEntity: reporteEntities){
            ReporteDto reporteDto = MoMapper.modelMapper().map(reporteEntity, ReporteDto.class);
            dtoList.add(reporteDto);
        }
        return dtoList;
    }

    private ReporteDto convertToReporteDto(final ReporteEntity reporteEntity) {
        return MoMapper.modelMapper().map(reporteEntity, ReporteDto.class);
    }
}
