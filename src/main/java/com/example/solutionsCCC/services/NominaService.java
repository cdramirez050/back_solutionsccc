package com.example.solutionsCCC.services;

import com.example.solutionsCCC.configuration.MoMapper;
import com.example.solutionsCCC.dtos.NominaDto;
import com.example.solutionsCCC.entitys.NominaEntity;
import com.example.solutionsCCC.repository.INominaRepository;
import com.example.solutionsCCC.request.NominaSaveRequest;
import com.example.solutionsCCC.request.NominaUpdateRequest;
import com.example.solutionsCCC.services.interfaces.INominaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class NominaService implements INominaService {

    @Autowired
    private INominaRepository iNominaRepository;


    @Override
    public void save(NominaSaveRequest nominaSaveRequest) {
        NominaEntity nominaEntity = MoMapper.modelMapper().map(nominaSaveRequest, NominaEntity.class);
        iNominaRepository.save(nominaEntity);
    }

    @Override
    public NominaDto update(NominaUpdateRequest nominaUpdateRequest, int id) {
        Optional<NominaEntity> optionalNominaEntity = iNominaRepository.findById(id);
        if(optionalNominaEntity.isPresent()){
            NominaEntity nominaEntity = optionalNominaEntity.get();
            nominaEntity.setNombre(nominaUpdateRequest.getNombre());
            nominaEntity.setTotalPagar(nominaUpdateRequest.getTotalPagar());
            iNominaRepository.save(nominaEntity);
            return MoMapper.modelMapper().map(nominaEntity, NominaDto.class);
        }
        return null;
    }

    @Override
    public void deleteById(int id) {
        iNominaRepository.deleteById(id);
    }


    @Override
    public NominaDto findById(int id) {
        Optional<NominaEntity> optionalNominaEntity = iNominaRepository.findById(id);
        NominaDto nominaDto = null;
        if(optionalNominaEntity.isPresent()){
            nominaDto = MoMapper.modelMapper().map(optionalNominaEntity.get(), NominaDto.class);
        }
        return nominaDto;
    }

    @Override
    public NominaDto findByNombre(String nombre) {
        Optional<NominaEntity> nomina = iNominaRepository.findByNombre(nombre);
        NominaDto nominaDto = null;
        if (nomina.isPresent()) {
            nominaDto = MoMapper.modelMapper().map(nomina.get(), NominaDto.class);
        }
        return nominaDto;
    }

    @Override
    public List<NominaDto> findAll() {
        List<NominaDto> dtoList = new ArrayList<>();
        Iterable<NominaEntity> nominaEntities = iNominaRepository.findAll();
        for(NominaEntity nominaEntity: nominaEntities){
            NominaDto nominaDto = MoMapper.modelMapper().map(nominaEntity, NominaDto.class);
            dtoList.add(nominaDto);
        }
        return dtoList;
    }

    private NominaDto convertToNominaDto(final NominaEntity nominaEntity) {
        return MoMapper.modelMapper().map(nominaEntity, NominaDto.class);
    }
}
