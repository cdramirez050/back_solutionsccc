package com.example.solutionsCCC.services;

import com.example.solutionsCCC.configuration.MoMapper;
import com.example.solutionsCCC.dtos.ClienteDto;
import com.example.solutionsCCC.entitys.ClienteEntity;
import com.example.solutionsCCC.repository.IClienteRepository;
import com.example.solutionsCCC.request.ClienteSaveRequest;
import com.example.solutionsCCC.request.ClienteUpdateRequest;
import com.example.solutionsCCC.services.interfaces.IClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ClienteService implements IClienteService {

    @Autowired
    private IClienteRepository iClienteRepository;


    @Override
    public void save(ClienteSaveRequest clienteSaveRequest) {
        ClienteEntity clienteEntity = MoMapper.modelMapper().map(clienteSaveRequest, ClienteEntity.class);
        iClienteRepository.save(clienteEntity);
    }

    @Override
    public void deleteById(int id) {
        iClienteRepository.deleteById(id);
    }


    @Override
    public ClienteDto update(ClienteUpdateRequest clienteUpdateRequest, int id) {
        Optional<ClienteEntity> optionalClienteEntity = iClienteRepository.findById(id);
        if(optionalClienteEntity.isPresent()){
            ClienteEntity clienteEntity = optionalClienteEntity.get();
            clienteEntity.setNombres(clienteUpdateRequest.getNombres());
            clienteEntity.setApellidos(clienteUpdateRequest.getApellidos());
            clienteEntity.setCorreo(clienteUpdateRequest.getCorreo());
            clienteEntity.setDireccion(clienteUpdateRequest.getDireccion());
            clienteEntity.setNumeroTelefono(clienteUpdateRequest.getNumeroTelefono());
            iClienteRepository.save(clienteEntity);
            return MoMapper.modelMapper().map(clienteEntity, ClienteDto.class);
        }
        return null;
    }

    @Override
    public ClienteDto findById(int id) {
        Optional<ClienteEntity> optionalClienteEntity = iClienteRepository.findById(id);
        ClienteDto clienteDto = null;
        if(optionalClienteEntity.isPresent()){
            clienteDto = MoMapper.modelMapper().map(optionalClienteEntity.get(), ClienteDto.class);
        }
        return clienteDto;
    }

    @Override
    public ClienteDto findByNumeroDocumento(String numeroDocumento) {
        Optional<ClienteEntity> cliente = iClienteRepository.findByNumeroDocumento(numeroDocumento);
        ClienteDto clienteDto = null;
        if (cliente.isPresent()) {
            clienteDto = MoMapper.modelMapper().map(cliente.get(), ClienteDto.class);
        }
        return clienteDto;
    }

    @Override
    public ClienteDto findByNombres(String nombres) {
        Optional<ClienteEntity> cliente = iClienteRepository.findByNombres(nombres);
        ClienteDto clienteDto = null;
        if (cliente.isPresent()) {
            clienteDto = MoMapper.modelMapper().map(cliente.get(), ClienteDto.class);
        }
        return clienteDto;
    }

    @Override
    public List<ClienteDto> findAll() {
        List<ClienteDto> dtoList = new ArrayList<>();
        Iterable<ClienteEntity> clienteEntities = iClienteRepository.findAll();
        for(ClienteEntity clienteEntity: clienteEntities){
            ClienteDto clienteDto = MoMapper.modelMapper().map(clienteEntity, ClienteDto.class);
            dtoList.add(clienteDto);
        }
        return dtoList;
    }

    private ClienteDto convertToClienteDto(final ClienteEntity clienteEntity) {
        return MoMapper.modelMapper().map(clienteEntity, ClienteDto.class);
    }
}
