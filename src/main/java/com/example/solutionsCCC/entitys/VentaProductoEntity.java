package com.example.solutionsCCC.entitys;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "ventas_productos")
public class VentaProductoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idVentaProducto;

    @Column(name = "cantidas_producto", nullable = false, length = 250)
    private String cantProducto;

    @Column(name = "relacion", nullable = false)
    private String pedidoProductoss;

    @ManyToMany
    @JoinTable(
            name = "venta_producto",
            joinColumns = @JoinColumn(name = "id_venta_producto"),
            inverseJoinColumns = @JoinColumn(name = "id_producto")
            //uniqueConstraints = @UniqueConstraint(columnNames={"id_empleado", "id_nomina"})
    )

    List<ProductoEntity> producto;


    public int getIdVentaProducto() {
        return idVentaProducto;
    }

    public void setIdVentaProducto(int idVentaProducto) {
        this.idVentaProducto = idVentaProducto;
    }

    public String getCantProducto() {
        return cantProducto;
    }

    public void setCantProducto(String cantProducto) {
        this.cantProducto = cantProducto;
    }

    public List<ProductoEntity> getProducto() {
        return producto;
    }

    public void setProducto(List<ProductoEntity> producto) {
        this.producto = producto;
    }

    public String getPedidoProductoss() {
        return pedidoProductoss;
    }

    public void setPedidoProductoss(String pedidoProductoss) {
        this.pedidoProductoss = pedidoProductoss;
    }
}
