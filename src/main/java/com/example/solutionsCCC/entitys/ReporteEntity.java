package com.example.solutionsCCC.entitys;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "reportes")
public class ReporteEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idReporte;

    @ManyToMany
    @JoinTable(
            name = "reporte_nomina",
            joinColumns = @JoinColumn(name = "id_reporte"),
            inverseJoinColumns = @JoinColumn(name = "id_empleado")
            //uniqueConstraints = @UniqueConstraint(columnNames={"id_empleado", "id_nomina"})
    )
    List<EmpleadoEntity> empleadoNomina;

    @ManyToMany
    @JoinTable(
            name = "reporte_pedido",
            joinColumns = @JoinColumn(name = "id_reporte"),
            inverseJoinColumns = @JoinColumn(name = "id_venta_producto")
            //uniqueConstraints = @UniqueConstraint(columnNames={"id_empleado", "id_nomina"})
    )
    List<VentaProductoEntity> pedidoProducto;

    public int getIdReporte() {
        return idReporte;
    }

    public void setIdReporte(int idReporte) {
        this.idReporte = idReporte;
    }

    public List<EmpleadoEntity> getEmpleadoNomina() {
        return empleadoNomina;
    }

    public void setEmpleadoNomina(List<EmpleadoEntity> empleadoNomina) {
        this.empleadoNomina = empleadoNomina;
    }

    public List<VentaProductoEntity> getPedidoProducto() {
        return pedidoProducto;
    }

    public void setPedidoProducto(List<VentaProductoEntity> pedidoProducto) {
        this.pedidoProducto = pedidoProducto;
    }
}
