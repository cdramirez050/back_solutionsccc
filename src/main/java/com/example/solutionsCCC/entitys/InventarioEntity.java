package com.example.solutionsCCC.entitys;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "inventarios")
public class InventarioEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idInventario;

    @Column(name = "fecha_inventario", nullable = false, length = 250)
    private String fechaInventario;

    @ManyToMany
    @JoinTable(
            name = "iventario_producto",
            joinColumns = @JoinColumn(name = "id_inventario"),
            inverseJoinColumns = @JoinColumn(name = "id_producto")
            //uniqueConstraints = @UniqueConstraint(columnNames={"id_empleado", "id_nomina"})
    )
    List<ProductoEntity> producto;

    public int getIdInventario() {
        return idInventario;
    }

    public void setIdInventario(int idInventario) {
        this.idInventario = idInventario;
    }

    public String getFechaInventario() {
        return fechaInventario;
    }

    public void setFechaInventario(String fechaInventario) {
        this.fechaInventario = fechaInventario;
    }

    public List<ProductoEntity> getProducto() {
        return producto;
    }

    public void setProducto(List<ProductoEntity> producto) {
        this.producto = producto;
    }
}
