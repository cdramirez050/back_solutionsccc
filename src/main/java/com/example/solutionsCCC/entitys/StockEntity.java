package com.example.solutionsCCC.entitys;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "stock")
public class StockEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idStock;

    @ManyToMany
    @JoinTable(
            name = "pedido_producto",
            joinColumns = @JoinColumn(name = "id_stock"),
            inverseJoinColumns = @JoinColumn(name = "id_pedido")
            //uniqueConstraints = @UniqueConstraint(columnNames={"id_empleado", "id_nomina"})
    )
    List<PedidoEntity> pedido;

    @ManyToMany
    @JoinTable(
            name = "producto_pedido",
            joinColumns = @JoinColumn(name = "id_stock"),
            inverseJoinColumns = @JoinColumn(name = "id_venta_producto")
            //uniqueConstraints = @UniqueConstraint(columnNames={"id_empleado", "id_nomina"})
    )

    List<VentaProductoEntity> producto;



    public int getIdStock() {
        return idStock;
    }

    public void setIdStock(int idStock) {
        this.idStock = idStock;
    }

    public List<VentaProductoEntity> getProducto() {
        return producto;
    }

    public void setProducto(List<VentaProductoEntity> producto) {
        this.producto = producto;
    }

    public List<PedidoEntity> getPedido() {
        return pedido;
    }

    public void setPedido(List<PedidoEntity> pedido) {
        this.pedido = pedido;
    }

}
