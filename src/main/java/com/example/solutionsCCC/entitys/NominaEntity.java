package com.example.solutionsCCC.entitys;

import javax.persistence.*;

@Entity
@Table(name = "nominas")
public class NominaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idNomina;

    @Column(name = "nombre", nullable = false, length = 250)
    private String nombre;

    @Column(name = "total_pagar", nullable = false, length = 20)
    private Double totalPagar;


    public int getIdNomina() {
        return idNomina;
    }

    public void setIdNomina(int idNomina) {
        this.idNomina = idNomina;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getTotalPagar() {
        return totalPagar;
    }

    public void setTotalPagar(Double totalPagar) {
        this.totalPagar = totalPagar;
    }

}
