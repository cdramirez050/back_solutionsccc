package com.example.solutionsCCC.entitys;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "pedidos")
public class PedidoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idPedido;

    @Column(name = "fecha_pedido", nullable = false, length = 30)
    private String fechaPedido;

    @Column(name = "fecha_entrega", nullable = false, length = 30)
    private String fechaEntrega;

    @Column(name = "descripcion", nullable = false, length = 250)
    private String descripcion;

    @Column(name = "cantidad", nullable = false, length = 15)
    private String cantidad;

    @Column(name = "valor_por_unidad", nullable = false, length = 15)
    private double valorUnidad;

    @Column(name = "valor_total", nullable = false, length = 15)
    private double valorTotal;

    @Column(name = "is_estado", nullable = false)
    private boolean isEstado;

    @ManyToMany
    @JoinTable(
            name = "pedido_cliente",
            joinColumns = @JoinColumn(name = "id_pedido"),
            inverseJoinColumns = @JoinColumn(name = "id_cliente")
            //uniqueConstraints = @UniqueConstraint(columnNames={"id_pedido", "id_cliente"})
    )
    List<ClienteEntity> cliente;

    public int getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(int idPedido) {
        this.idPedido = idPedido;
    }

    public String getFechaPedido() {
        return fechaPedido;
    }

    public void setFechaPedido(String fechaPedido) {
        this.fechaPedido = fechaPedido;
    }

    public String getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(String fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public double getValorUnidad() {
        return valorUnidad;
    }

    public void setValorUnidad(double valorUnidad) {
        this.valorUnidad = valorUnidad;
    }

    public double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public List<ClienteEntity> getCliente() {
        return cliente;
    }

    public void setCliente(List<ClienteEntity> cliente) {
        this.cliente = cliente;
    }

    public boolean isEstado() {
        return isEstado;
    }

    public void setEstado(boolean estado) {
        isEstado = estado;
    }
}
