package com.example.solutionsCCC.entitys;

import javax.persistence.*;

@Entity
@Table(name = "loggin")
public class LoginEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idLoggin;

    @Column(name = "usuario", nullable = false, length = 250)
    private String usuario;

    @Column(name = "password", nullable = false, length = 20)
    private String password;

    @Column(name = "estado", nullable = false, length = 20)
    private Boolean estado;

    public int getIdLoggin() {
        return idLoggin;
    }

    public void setIdLoggin(int idLoggin) {
        this.idLoggin = idLoggin;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }
}
