package com.example.solutionsCCC.entitys;

import javax.persistence.*;

@Entity
@Table(name = "productos")
public class ProductoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idProducto;

    @Column(name = "nombre", nullable = false, length = 250)
    private String nombre;

    @Column(name = "descripcion", nullable = false, length = 250)
    private String descripcion;

    @Column(name = "cantidad", nullable = false, length = 15)
    private String cantidad;

    @Column(name = "fecha_compra", nullable = false, length = 20)
    private String fechaCompra;

    @Column(name = "v_total_producto", nullable = false, length = 15)
    private double vTotalProducto;

    @Column(name = "v_unidad_costo", nullable = false, length = 15)
    private double vUnidadCosto;

    @Column(name = "v_unidad_venta", nullable = false, length = 15)
    private double vUnidadVenta;

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public double getvTotalProducto() {
        return vTotalProducto;
    }

    public void setvTotalProducto(double vTotalProducto) {
        this.vTotalProducto = vTotalProducto;
    }

    public double getvUnidadCosto() {
        return vUnidadCosto;
    }

    public void setvUnidadCosto(double vUnidadCosto) {
        this.vUnidadCosto = vUnidadCosto;
    }

    public double getvUnidadVenta() {
        return vUnidadVenta;
    }

    public void setvUnidadVenta(double vUnidadVenta) {
        this.vUnidadVenta = vUnidadVenta;
    }

    public String getFechaCompra() {
        return fechaCompra;
    }

    public void setFechaCompra(String fechaCompra) {
        this.fechaCompra = fechaCompra;
    }
}
