package com.example.solutionsCCC.configuration;

import org.modelmapper.ModelMapper;

public class MoMapper {

    public static ModelMapper modelMapper() {
        return new ModelMapper();
    }
}
